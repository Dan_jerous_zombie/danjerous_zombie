﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour {

    public int HP;
    bool invisibleFlag;
    float blinkTimer;
    float blinkInterval = 0.1f;
    float invisibleTimer;
    float invisibleInterval = 0.5f;


    // Use this for initialization
    void Start()
    {
        HP = 100;
        invisibleFlag = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(!invisibleFlag)
        transform.Translate(new Vector3(0.1f, 0, 0));

        if (invisibleFlag)
        {
            Renderer playerRenderer = GetComponent<Renderer>();

            blinkTimer += Time.deltaTime;
            if (blinkTimer > blinkInterval)
            {
                // 点滅させる
                playerRenderer.enabled = !playerRenderer.enabled;
                blinkTimer = 0;
            }

            invisibleTimer += Time.deltaTime;
            if (invisibleTimer > invisibleInterval)
            {
                // 無敵時間終了
                invisibleTimer = 0;
                invisibleFlag = false;

                playerRenderer.enabled = true;
            }
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Enemy" && !invisibleFlag )
        {
            GameObject enemy = collision.gameObject;
            EnemyManager enemyManagerScript;
            enemyManagerScript = enemy.GetComponent<EnemyManager>();
            int Point = enemyManagerScript.damagePoint;
            HP -= Point;
            if (HP < 0) HP = 0;
            Debug.Log("Hit!");
            invisibleFlag = true;
            knockBack(enemy);
        }

    }

    private void knockBack(GameObject enemy)
    {
        // 体当たりしてきた敵とプレイヤーの座標からノックバックする方向を取得する
        Vector3 knockBackDirection = (enemy.transform.position - transform.position).normalized;

        // ノックバックの方向を逆転させる
        knockBackDirection.x *= -1;
        knockBackDirection.y = 1;

        // ノックバックさせる
        Rigidbody rb = GetComponent<Rigidbody>();
        rb.AddForce(knockBackDirection * 100);
        Debug.Log("knockback");
    }
}
