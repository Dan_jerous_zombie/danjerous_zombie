﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {

    Slider slider;
    int HPvalue;
    PlayerManager playerManager;

	// Use this for initialization
	void Start () {
        slider = GameObject.Find("HPbar").GetComponent<Slider>();
        playerManager = GameObject.Find("Player").GetComponent<PlayerManager>();

	}
	
	// Update is called once per frame
	void Update () {
        HPvalue = playerManager.HP;
        slider.value = HPvalue;
	}
}
